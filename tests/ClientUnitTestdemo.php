<?php

namespace gitlab\tests;
use App\Entity\Client;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ClientUnitTestdemo extends KernelTestCase
{
   
    public function testValidEntity(){
        $client=(new Client())
        ->setNom("Sam")
        ->setPrenom("Med")
        ->setCin(15487421)
        ->setAdresse("Tunis, Tunisie");
        self::bootKernel();
        $error = self ::$container->get('validator')->validate($client);
        $this->assertCount(0, $error);


    }
    
}
