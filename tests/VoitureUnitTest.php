<?php

namespace App\Tests;
use App\Entity\Voiture;
use PHPUnit\Framework\TestCase;
use App\Tests\DateTime;

class VoitureUnitTest extends TestCase
{
    public function testSomething(): void
    {
        $this->assertTrue(true);
    }
     public function testEntityVoiture(): void
    {
        $voiture=new Voiture();
        $this->assertInstanceOf(Voiture::class, $voiture);
    }

    public function testIsTrueVoiture()
    {
        $datetime = new \DateTime();
        $voiture = new Voiture();
        $voiture->setSerie("209TU4365") 
                ->setDateMiseEnMarche ($datetime)
                ->setModele('MB')
                ->setPrixJour(20.5);

        $this->assertTrue($voiture ->getSerie() === "209TU4365"); 
        $this->assertTrue($voiture ->getDateMiseEnMarche() === $datetime); 
        $this->assertTrue($voiture->getModele() === 'MB');
        $this->assertTrue($voiture->getPrixJour() === 20.5);
    }

    public function testIsFalseVoiture()
    {
        $datetime = new \DateTime();
        $voiture = new Voiture();
        $voiture->setSerie("209TU4365") 
                ->setDateMiseEnMarche ($datetime)
                ->setModele('MB')
                ->setPrixJour(20.5);

        $this->assertFalse($voiture ->getSerie() === "209TU9965"); 
        $this->assertFalse($voiture ->getDateMiseEnMarche() === 'falseDate'); 
        $this->assertFalse($voiture->getModele() === 'BMW');
        $this->assertFalse($voiture->getPrixJour() === 70.5);
    }
}

