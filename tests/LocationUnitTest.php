<?php

namespace App\Tests;
use App\Entity\Location;
use App\Entity\Voiture;
use App\Entity\Client;
use PHPUnit\Framework\TestCase;
use App\Tests\DateTime;

class LocationUnitTest extends TestCase
{
    public function testSomething(): void
    {
        $this->assertTrue(true);
    }
     public function testEntityLocation(): void
    {
        $location=new Location();
        $this->assertInstanceOf(Location::class, $location);
    }

    public function testIsTrueLocation()
    {
        $datetime = new \DateTime();
        $location = new Location();
        $client = new Client();
        $voiture = new Voiture();
        $location->setDateDebut($datetime)
                 ->setDateRetour($datetime)
                 ->setPrix(75.5)
                 ->setClient($client)
                 ->setVoiture($voiture);
                 

        $this->assertTrue($location->getDateDebut() === $datetime);
        $this->assertTrue($location->getDateRetour() === $datetime);
        $this->assertTrue($location->getPrix() === 75.5);
        $this->assertTrue($location->getClient() === $client);
        $this->assertTrue($location->getVoiture() === $voiture);

        
    }

    public function testIsFalseLocation()
    {
        $datetime = new \DateTime();
        $client = new Client();
        $voiture = new Voiture();
        $location = new Location();
        $location->setDateDebut($datetime)
                 ->setDateRetour($datetime)
                 ->setPrix(75.5)
                 ->setClient($client)
                 ->setVoiture($voiture);

        $this->assertFalse($location->getDateDebut() === "false");
        $this->assertFalse($location->getDateRetour() === "false");
        $this->assertFalse($location->getPrix() === 00.5);
        $this->assertFalse($location->getClient() === "Fasle");
        $this->assertFalse($location->getVoiture() === "Fasle");


    }
}


